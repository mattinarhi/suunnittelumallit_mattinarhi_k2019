/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public abstract class BurgerBuilder {
    
    protected Burger burger;
    
    public Burger getBurger() {
        return burger;
    }
    public void createNewBurgerProduct() {
        burger = new Burger();
    }
    public abstract void buildSampyla();
    public abstract void buildPihvi();
    public abstract void buildLisuke();
}

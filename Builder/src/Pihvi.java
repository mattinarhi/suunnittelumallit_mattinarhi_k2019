/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Pihvi extends Burger {
    private String nimi;
    
    public Pihvi(String nimi) {
        this.nimi = nimi;
        toString();
    }
    public String toString() {
        return " " + nimi;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Tarjoilija {
    
    private BurgerBuilder builder;
    
    public void setBurgerBuilder(BurgerBuilder builder) {
        this.builder = builder;
    }
    public Burger getBurger() {
        return builder.getBurger();
    }
    public void constructBurger() {
        builder.createNewBurgerProduct();
        builder.buildSampyla();
        builder.buildPihvi();
        builder.buildLisuke();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public abstract class Game {
    
    protected int playersCount;
    
    public abstract void initializeGame();
    
    public abstract void makePlay(int player);
    
    public abstract boolean endOfGame();
    
    public abstract void printWinner();
    
    public final void playOneGame(int playersCount) {
        this.playersCount = playersCount;
        initializeGame();
        int j = 0;
        while(!endOfGame()) {
            makePlay(j);
            j = (j+1) % playersCount;
        
        printWinner();
    }
    } 
}

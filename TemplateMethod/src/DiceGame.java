/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class DiceGame extends Game {
    
    private int luvut [] = new int[2];
    private String result;
    
    public DiceGame() {
        result = null;
    }
    
    
    @Override
    public void initializeGame() {
        System.out.println("The game has started");
    }
    @Override
    public void makePlay(int player) {
        int number = (int)(Math.random()*6+1);
        luvut[player] = number;
        
    }
    @Override
    public boolean endOfGame() {
        if(luvut[0] == 0 || luvut[1] == 0) {
            
            return false;
        }
        else if (luvut[0]>luvut[1]){
            result = "player 1";
            
            
            
            return true;
        }
        else {
            result = "player 2";
            return true;
        }
             
    }
    @Override
    public void printWinner() {
        endOfGame();
        if(result != null) {
        System.out.println("The winner is: " + result);
            System.out.println("Number for player 1: " + luvut[0]);
            System.out.println("Number for player 2: " + luvut[1]);
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Owl implements Bird {
    @Override
    public void fly() {
        System.out.println("Owl is flying");
    }
    @Override
    public void makeSound() {
        System.out.println("Hu huu");
    }
}

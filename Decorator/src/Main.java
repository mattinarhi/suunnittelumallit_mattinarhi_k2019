/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    
    public static void main(String[] args) {
        
        Pizza_IF pizza = new JauhelihaPizza();
        
        pizza = new Jauheliha(pizza);
        
        System.out.println(pizza.getKuvaus() + ",hinta:" + pizza.getHinta() + " €");
        
        pizza = new SalamiPizza();
        
        pizza = new Salami(pizza);
        pizza = new Mozzarella(pizza);
        System.out.println(pizza.getKuvaus() + ",hinta: " + pizza.getHinta() + " €");
        
        pizza = new KinkkuPizza();
        
        pizza = new Kinkku(pizza);
        pizza = new Mozzarella(pizza);
        System.out.println(pizza.getKuvaus() + ",hinta: " + pizza.getHinta() + " €");
    }
    
}

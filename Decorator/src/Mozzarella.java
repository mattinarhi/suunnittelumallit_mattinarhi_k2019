/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Mozzarella extends PizzaDecorator {
    
    private final Pizza_IF pizza;
    
    public Mozzarella(Pizza_IF pizza) {
        this.pizza = pizza;
    }
    @Override
    public String getKuvaus() {
     return pizza.getKuvaus() + " ,mozzarella";
    }
    @Override
    public double getHinta() {
        return pizza.getHinta() + 4.00;
    }
}

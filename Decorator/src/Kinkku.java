/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Kinkku extends PizzaDecorator {
    
    private final Pizza_IF pizza;
    
    public Kinkku(Pizza_IF pizza) {
        this.pizza = pizza;
    }
    @Override
    public String getKuvaus() {
        return pizza.getKuvaus() + " , kinkku";
    }
    @Override
    public double getHinta() {
        return pizza.getHinta() + 4.00;
    }
}

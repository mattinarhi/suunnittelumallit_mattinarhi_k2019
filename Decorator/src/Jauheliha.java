/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Jauheliha extends PizzaDecorator {
    
    private final Pizza_IF pizza;
    
    public Jauheliha(Pizza_IF pizza) {
        this.pizza = pizza;
    }
    @Override
    public String getKuvaus() {
        return pizza.getKuvaus() + " , jauheliha";
    }
    @Override
    public double getHinta() {
        return pizza.getHinta() + 7.00;
    }
}

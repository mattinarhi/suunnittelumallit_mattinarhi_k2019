/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Matti
 */
public class Kello implements Cloneable {
    
    private Tuntiviisari hours;
    private MinViisari minutes;
    private SekViisari seconds;
    
    public Kello(int tunnit, int minuutit, int sekunnit) {
        hours = new Tuntiviisari(tunnit);
        minutes = new MinViisari(minuutit);
        seconds = new SekViisari(sekunnit);
        
        
    }
    
    public void printAika() {
        
        System.out.println("Aika: " + hours.getTunnit()+ ":" + minutes.getMinuutit() + ":" + seconds.getSekunnit());
        
    }
    
    public void muutaAikaa(int tunnit, int minuutit, int sekunnit) {
        hours.setTunnit(tunnit);
        minutes.setMinuutit(minuutit);
        seconds.setSekunnit(sekunnit);
    }
    @Override
    public Kello clone() {
        Kello k = null;
        try {
           k = (Kello)super.clone();
           k.hours = (Tuntiviisari)hours.clone();
           k.minutes = (MinViisari)minutes.clone();
           k.seconds = (SekViisari)seconds.clone();
        }
        catch(CloneNotSupportedException e) {
            System.out.println("Ei onnistunut");
            
        }
        return k;
    }
}
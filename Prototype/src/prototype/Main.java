/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Matti
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Kello kello = new Kello(23,59,40);
        Kello kello1 = (Kello)kello.clone();
        kello.printAika();
        kello1.printAika();
        kello1.muutaAikaa(22, 59, 30);
        kello.printAika();
        kello1.printAika();
        kello.muutaAikaa(23, 12, 15);
        kello.printAika();
        kello1.printAika();
    }
    
}

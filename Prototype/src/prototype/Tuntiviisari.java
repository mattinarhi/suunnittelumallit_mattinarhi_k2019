/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prototype;

/**
 *
 * @author Matti
 */
public class Tuntiviisari implements Cloneable {
    
    private int tunnit;
    
    public Tuntiviisari(int tunnit) {
        this.tunnit = tunnit;
    }
    
    public int getTunnit() {
        return tunnit;
    }
    
    public void setTunnit(int tunnit) {
        this.tunnit = tunnit;
    }
    
    @Override
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch(CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}

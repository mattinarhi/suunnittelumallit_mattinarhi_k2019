/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    public static void main(String[] args) {
        Screen screen = new Screen();
        ScreenUpCommand command1 = new ScreenUpCommand(screen);
        ScreenDownCommand command2 = new ScreenDownCommand(screen);
        WallButton button1 = new WallButton(command1);
        WallButton button2 = new WallButton(command2);
        button1.push();
        button2.push();
    }
}

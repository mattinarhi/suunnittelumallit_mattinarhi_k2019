/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class ScreenUpCommand implements Command{
    
    private Screen screen;
    
    public ScreenUpCommand(Screen screen) {
        this.screen = screen;
    }
    @Override
    public void execute() {
        screen.screenUp();
        
    }
    
    
}

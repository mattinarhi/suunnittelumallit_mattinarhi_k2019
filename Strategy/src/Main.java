/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    public static void main(String[] args) {
        int[] arr = { 4, 2, 9, 6, 23, 12, 34, 0, 1 };
        SortContext context = new SortContext();
        context.setSortStrategy(new SelectionSort());
        context.sort(arr);
        context.setSortStrategy(new MergeSort());
        context.sort(arr);
        context.setSortStrategy(new QuickSort());
        context.sort(arr);
    }
}

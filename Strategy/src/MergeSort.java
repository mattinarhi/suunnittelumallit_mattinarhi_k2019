/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class MergeSort implements SortStrategy {
    
    private int[] array;
    private int[] tempMergArr;
    private int length;
    private long counter;
    
    @Override
    public void doSort(int [] arr) {
        sort(arr);
    }
    
    public void sort(int inputArr[]) {
        this.array = inputArr;
        this.length = inputArr.length;
        this.tempMergArr = new int[length];
        doMergeSort(0, length - 1);
        System.out.println("Amount of comparisons in MergeSort : " + counter);
        System.out.print("Result of Mergesort: ");
        for(int i = 0; i<tempMergArr.length; i++) {
            System.out.print(tempMergArr[i] + ", ");
        }
        System.out.println("\n");
    }
 
    private void doMergeSort(int lowerIndex, int higherIndex) {
        counter++;
        if (lowerIndex < higherIndex) {
            int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
            // Below step sorts the left side of the array
            doMergeSort(lowerIndex, middle);
            // Below step sorts the right side of the array
            doMergeSort(middle + 1, higherIndex);
            // Now merge both sides
            mergeParts(lowerIndex, middle, higherIndex);
        }
        counter++;
    }
 
    private void mergeParts(int lowerIndex, int middle, int higherIndex) {
        
        counter++;
        for (int i = lowerIndex; i <= higherIndex; i++) {
            tempMergArr[i] = array[i];
        }
        counter++;
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        counter++;
        while (i <= middle && j <= higherIndex) {
            counter++;
            if (tempMergArr[i] <= tempMergArr[j]) {
                array[k] = tempMergArr[i];
                i++;
            } else {
                array[k] = tempMergArr[j];
                j++;
            }
            counter++;
            k++;
        }
        counter++;
        while (i <= middle) {
            array[k] = tempMergArr[i];
            k++;
            i++;
        }
        counter++;
    }
}
    


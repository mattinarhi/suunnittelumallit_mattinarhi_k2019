/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class SelectionSort implements SortStrategy {
    
    private static long counter;
    
    public SelectionSort() {
        counter = 0;
    }
    
    @Override
    public void doSort(int [] arr) {
        doSelectionSort(arr);
    }
    
    public static int[] doSelectionSort(int[] arr){
        
        counter ++;
        for (int i = 0; i < arr.length - 1; i++)
        {
            int index = i;
            counter++;
            counter++;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index]) 
                    index = j;
            counter++;
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        counter++;
        System.out.println("Amount of comparisons in SelectionSort: " + counter);
        System.out.print("Result of SelectionSort:");
        for(int i = 0; i<arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println("\n");
        return arr;
    }
     
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class SortContext {
    private SortStrategy strategy;
    
    public void setSortStrategy(SortStrategy strategy) {
        this.strategy = strategy;
    }
    public void sort(int[] arr) {
        strategy.doSort(arr);
    }
}

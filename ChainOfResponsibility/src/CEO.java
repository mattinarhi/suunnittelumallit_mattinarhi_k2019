/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class CEO extends Chain {
    
    @Override
    public void handleRequest(RaiseRequest request) {
        if(request.getRaise()/BASE <= 1.10) {
            System.out.println("CEO approved your raise request.");
        }
        else {
            System.out.println("Your raise request wasn't approved because it was too high.");
        }
    }
    
    
}

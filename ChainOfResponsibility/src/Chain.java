/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public abstract class Chain {
    protected static final double BASE = 2500.0;
    protected Chain nextInChain;
    
    public void setNext(Chain nextInChain) {
        this.nextInChain = nextInChain;
    }
    public abstract void handleRequest(RaiseRequest request);
    
   
}

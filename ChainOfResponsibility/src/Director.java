/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Director extends Chain {
    
    @Override
    public void handleRequest(RaiseRequest request) {
        if(request.getRaise()/BASE < 1.05) {
            System.out.println("Director approved your raise request.");
        }
        else {
            nextInChain.handleRequest(request);
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager();
        Director director = new Director();
        CEO ceo = new CEO();
        manager.setNext(director);
        director.setNext(ceo);
        
        manager.handleRequest(new RaiseRequest(2525.0));
        manager.handleRequest(new RaiseRequest(2600.0));
        manager.handleRequest(new RaiseRequest(2700.0));
        manager.handleRequest(new RaiseRequest(10000.0));
    }
}

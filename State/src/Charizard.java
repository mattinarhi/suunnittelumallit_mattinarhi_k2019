/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Charizard implements PokemonState {
    
    public Charizard(Context context) {
        context.setState(this);
    }
    
    @Override
    public String tellName() {
        return "Charizard";
    }
    @Override
    public double getAttackPoints() {
        return 30.0;
                
    }
    @Override
    public double getHealPoints() {
     
    return 15.0;
    }
    
}

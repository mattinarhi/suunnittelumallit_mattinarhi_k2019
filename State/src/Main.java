/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    
    public static void main(String[] args) {
        Context context = new Context();
        Charmander charmander = new Charmander(context);
        
        System.out.println("Name: " + context.getState().tellName());
        System.out.println("Damage dealt: " + context.getState().getAttackPoints() + " points.");
        System.out.println("Regained " + context.getState().getHealPoints() + " hp");
        System.out.println("\n\n\n");
        
        Charmeleon charmeleon = new Charmeleon(context);
        System.out.println("Name: " + context.getState().tellName());
        System.out.println("Damage dealt: " + context.getState().getAttackPoints() + " points.");
        System.out.println("Regained " + context.getState().getHealPoints() + " hp");
        System.out.println("\n\n\n");
        
        Charizard charizard = new Charizard(context);
        System.out.println("Name: " + context.getState().tellName());
        System.out.println("Damage dealt: " + context.getState().getAttackPoints() + " points.");
        System.out.println("Regained " + context.getState().getHealPoints() + " hp");
        
    }  
}

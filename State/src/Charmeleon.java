/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Charmeleon implements PokemonState {
    
    
    public Charmeleon(Context context) {
        context.setState(this);
    }
    
    @Override
    
    public String tellName() {
       
        return "Charmeleon";
    }
    @Override
    public double getAttackPoints() {
        
        return 20.0;
    }
    @Override
    public double getHealPoints() {
        return 10.0;
    }
    
}

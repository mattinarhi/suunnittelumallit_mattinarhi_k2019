/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasFactory implements AbstractFactory {
    
    @Override
    public TPaita createTPaita () {
        return new AdidasPaita();
    }
    @Override
    public Lippis createLippis () {
        return new AdidasLippis();
    }
    @Override
    public Kengat createKengat () {
        return new AdidasKengat();
    }
    @Override
    public Farkut createFarkut () {
        return new AdidasFarkut();
    }
}

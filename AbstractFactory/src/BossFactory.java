/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossFactory implements AbstractFactory {
    
    @Override
    public Lippis createLippis() {
        return new BossLippis();
    }
    @Override
    public TPaita createTPaita () {
        return new BossPaita();
    }
    @Override
    public Farkut createFarkut () {
        return new BossFarkut ();
            
            
        }
    @Override
    public Kengat createKengat() {
        return new BossKengat();
    }
}
    


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public interface AbstractFactory {
    
    public abstract TPaita createTPaita();
    public abstract Lippis createLippis();
    public abstract Farkut createFarkut();
    public abstract Kengat createKengat();
}

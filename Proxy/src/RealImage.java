/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class RealImage implements Image {
    private final String filename;
    private ProxyImage image;
    
    public RealImage(String filename) {
        this.filename = filename;
        loadImageFromDisk();
    }
    
    private void loadImageFromDisk() {
        System.out.println("Loading " + filename);
        
    }
    @Override
    public void displayImage() {
        System.out.println("Displaying " + filename);
        
    }
    @Override
    public void showData() {
        
        
    }
}

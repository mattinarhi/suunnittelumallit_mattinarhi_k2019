
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
    
    public static void main(String[] args) {
       ArrayList<Image> images = new ArrayList<>();
        
        images.add(new ProxyImage("Testi1"));
        images.add(new ProxyImage("Testi2"));
        images.add(new ProxyImage("Testi3"));
        
        for(int i=0; i<images.size(); i++) {
            
            images.get(i).showData();
            images.get(i).displayImage();
            
        }
         
    }
    
}

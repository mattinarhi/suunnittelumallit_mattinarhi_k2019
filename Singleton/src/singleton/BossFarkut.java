package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossFarkut implements Farkut {
    
    private static BossFarkut INSTANCE = null;
    
    private BossFarkut() {
        
    }
    
    public static synchronized BossFarkut getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BossFarkut();
        }
        return INSTANCE;
    }

    public String toString () {
        return "Bossin farkut";
    }
    
    
}

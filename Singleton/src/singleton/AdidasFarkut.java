package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasFarkut implements Farkut {
    
    private static AdidasFarkut INSTANCE = null;
    
    public String toString () {
        return "Adidaksen farkut";
    }
    
    private AdidasFarkut() {
        
    }
    public static synchronized AdidasFarkut getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new AdidasFarkut();
        }
        return INSTANCE;
    }
}

package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasPaita implements TPaita {
    
    private static AdidasPaita INSTANCE = null;
    
    public String toString() {
        return "Adidaksen t-paita";
    }
    private AdidasPaita() {
        
    }
    
    public static synchronized AdidasPaita getInstance () {
        if(INSTANCE == null) {
            INSTANCE = new AdidasPaita();
        }
        return INSTANCE;
    }
    
}

package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasLippis implements Lippis {
   
    private static AdidasLippis INSTANCE = null;
    
    private AdidasLippis () {
        
    }
    
    public static synchronized AdidasLippis getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new AdidasLippis();
        }
        return INSTANCE;
    }
    public String toString () {
        return "Adidaksen lippis";
    }
    
}

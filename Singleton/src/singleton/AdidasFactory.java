package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasFactory implements AbstractFactory {
    
    private static AdidasFactory INSTANCE = null;
    
    private AdidasFactory() {
        
    }
    public static synchronized AdidasFactory getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new AdidasFactory();
        }
        return INSTANCE;
    }
    
    @Override
    public TPaita createTPaita () {
        return AdidasPaita.getInstance();
    }
    @Override
    public Lippis createLippis () {
        return AdidasLippis.getInstance();
    }
    @Override
    public Kengat createKengat () {
        return AdidasKengat.getInstance();
    }
    @Override
    public Farkut createFarkut () {
        return AdidasFarkut.getInstance();
    }
}

package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossPaita implements TPaita {
    
    private static BossPaita INSTANCE = null;
    
    private BossPaita () {
        
    }
    
    public static synchronized BossPaita getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BossPaita();
        }
        return INSTANCE;
    }
    
    public String toString () {
        return "Bossin t-paita";
    }
    
}

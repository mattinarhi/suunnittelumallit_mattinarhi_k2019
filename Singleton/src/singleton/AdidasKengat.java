package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class AdidasKengat implements Kengat {
    
    private static AdidasKengat INSTANCE = null;
    
    private AdidasKengat() {
        
    }
    
    public static synchronized AdidasKengat getInstance () {
        if(INSTANCE == null) {
            INSTANCE = new AdidasKengat();
        }
        return INSTANCE;
    }
    
    public String toString () {
        return "Adidaksen kengät";
    }
    
}

package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossLippis implements Lippis {
    
    private static BossLippis INSTANCE = null;
    
    private BossLippis() {
        
    }
    public static synchronized BossLippis getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BossLippis();
        }
        return INSTANCE;
    }
    public String toString () {
        return "Bossin lippis";
    }
    
}

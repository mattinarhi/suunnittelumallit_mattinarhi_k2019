package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Main {
   
    public static void main(String[] args) {
        AbstractFactory factory;
        factory = AdidasFactory.getInstance();
        System.out.println("Jasperilla on: " + factory.createTPaita() + ", "+factory.createFarkut()+ " , "+factory.createKengat()+" , "+factory.createLippis());
        factory = BossFactory.getInstance();
        System.out.println("Jasperilla on: " + factory.createTPaita() + ", "+factory.createFarkut()+ " , "+factory.createKengat()+" , "+factory.createLippis());
        
        
    }
    
}

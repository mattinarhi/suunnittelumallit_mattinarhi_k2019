package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossFactory implements AbstractFactory {
    
    public static BossFactory INSTANCE = null;
    
    private BossFactory() {
        
    }
    public static synchronized BossFactory getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BossFactory();
        }
        return INSTANCE;
    }
    
    @Override
    public Lippis createLippis() {
        return BossLippis.getInstance();
    }
    @Override
    public TPaita createTPaita () {
        return BossPaita.getInstance();
    }
    @Override
    public Farkut createFarkut () {
        return BossFarkut.getInstance();
            
            
        }
    @Override
    public Kengat createKengat() {
        return BossKengat.getInstance();
    }
}
    


package singleton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BossKengat implements Kengat {
    
    private static BossKengat INSTANCE = null;
    
    private BossKengat() {
        
    }
    
    public static synchronized BossKengat getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new BossKengat();
        }
        return INSTANCE;
    }
    
    public String toString () {
        return "Bossin kengät";
    }
    
}

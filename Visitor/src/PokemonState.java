/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public abstract class PokemonState {
   public abstract String tellName();
   public abstract double getAttackPoints();
   public abstract double getHealPoints();
   public abstract void accept(PokemonVisitor visitor);
}

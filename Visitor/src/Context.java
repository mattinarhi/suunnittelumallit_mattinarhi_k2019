/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Context {
    
    private PokemonState state;
    
    public Context() {
        state = null;
    }
    public void setState(PokemonState state) {
        this.state = state;
    }
    
    public PokemonState getState() {
        return state;
    
    }
}

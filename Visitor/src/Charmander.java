/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class Charmander extends PokemonState {
    
    public Charmander(Context context) {
        context.setState(this);
    }
    
    @Override
    public String tellName() {
        
        return "Charmander";
    }
    @Override
    public double getAttackPoints() {
        
        return 10.0;
    }
    @Override
    public double getHealPoints() {
        return 5.0;
    }
    @Override
    public void accept(PokemonVisitor visitor) {
        visitor.visit(this);
    }
   
}

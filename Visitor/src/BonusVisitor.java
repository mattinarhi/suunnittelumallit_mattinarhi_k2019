/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public class BonusVisitor implements PokemonVisitor {
    
    @Override
    public void visit(Charmander charmander) {
        System.out.println("5 bonus points has been added!");
    }
    @Override
    public void visit(Charmeleon charmeleon) {
        System.out.println("10 bonus points has been added!");
    }
    @Override
    public void visit(Charizard charizard) {
        System.out.println("15 bonus points has been added");
    }
}

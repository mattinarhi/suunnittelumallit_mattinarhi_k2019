/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matti
 */
public interface PokemonVisitor {
    public abstract void visit(Charmander charmander);
    public abstract void visit(Charmeleon charmeleon);
    public abstract void visit(Charizard charizard);
    
    
}
